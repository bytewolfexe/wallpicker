#ifndef SWAYIPC_HPP
#define SWAYIPC_HPP

#include <cstdint>
#include <vector>


namespace swayipc {
	enum class SwayIPC_MessageType : std::uint32_t {
		RUN_COMMAND = 0,
		GET_WORKSPACES,
		SUBSCRIBE,
		GET_OUTPUTS,
		GET_TREE,
		GET_MARKS,
		GET_BAR_CONFIG,
		GET_VERSION,
		GET_BINDING_MODES,
		GET_CONFIG,
		SEND_TICK,
		SYNC,
		GET_BINDING_STATE,
		GET_INPUTS,
		GET_SEATS,
	};

	class SwayIPC_Message {
	public:
		SwayIPC_Message(SwayIPC_MessageType type) noexcept;

		SwayIPC_Message(SwayIPC_MessageType type, const std::vector<std::uint8_t> &data) noexcept;

		const std::vector<std::uint8_t> &get_content() const noexcept;
	private:
		SwayIPC_Message() noexcept;

		std::vector<std::uint8_t> m_content;
	};


	struct SwayIPC_Response {
		std::uint32_t length;
		SwayIPC_MessageType type;
		std::vector<std::uint8_t> content;
	};


	class SwayIPC {
	public:
		SwayIPC() noexcept;
		~SwayIPC() noexcept;

		// Raw message send.
		void operator << (const std::vector<std::uint8_t> &message);

		// Message class send.
		void operator << (const SwayIPC_Message &message);

		void operator >> (SwayIPC_Response &response);

		bool is_ready() const noexcept;
	private:
		bool m_ready;

		bool m_written;

		int m_socket;
	};
}

#endif // SWAYIPC_HPP
