#include "swayipc.hpp"

#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <random>
#include <ratio>
#include <thread>
#include <vector>


namespace fs = std::filesystem;

using fs_opt = fs::directory_options;


bool is_image(const fs::path &path) {
	if(!path.has_extension()) {
		return false;
	}
	auto ext = path.extension();

	return ext == ".jpg" || ext == ".png" || ext == ".jpeg";
}

int main(int, char **) {
	const std::string scaling = "fill";

	const char *home = getenv("HOME");

	if(home == nullptr) {
		std::cerr << "Could not retrieve HOME environment variable!\n";
		return 1;
	}
	
	auto path = fs::path{home};
	path.append(".wallpapers");

	if(!fs::exists(path)) {
		std::cerr << path << " does not exist!\n";
		return 1;
	}

	auto directory = fs::recursive_directory_iterator{path, fs_opt::skip_permission_denied | fs_opt::follow_directory_symlink};

	auto entries = std::vector<fs::path>{};

	for(auto &dir : directory) {
		if(dir.is_regular_file()) {
			if(is_image(dir)) {
				entries.emplace_back(dir);
			}
		}
		else if(dir.is_symlink()) {
			auto p = fs::read_symlink(dir);
			if(is_image(p)) {
				entries.emplace_back(p);
			}
		}
	}

	if(entries.empty()) {
		std::cerr << "No image files to use as wallpapers were found!\n";
		return 1;
	}

	auto ipc = swayipc::SwayIPC{};

	if(!ipc.is_ready()) {
		std::cerr << "Failed to connect to i3 / sway IPC!\n";
		return 1;
	}

	auto rng = std::random_device{};

	std::shuffle(entries.begin(), entries.end(), rng);

	std::this_thread::sleep_for(std::chrono::duration<double, std::milli>{700.0});

	auto payload_str = std::string{"output * bg "};
	payload_str.append(std::string{entries.front()});
	payload_str.push_back(' ');
	payload_str.append(scaling);

	auto payload = std::vector<std::uint8_t>{};

	for(const auto &c : payload_str) {
		if(c != '\0') {
			payload.push_back(static_cast<std::uint8_t>(c));
		}
	}

	auto msg = swayipc::SwayIPC_Message{swayipc::SwayIPC_MessageType::RUN_COMMAND, payload};

	ipc << msg;

	auto res = swayipc::SwayIPC_Response{};

	ipc >> res;

	// No need to do anything else (I hope ...)

	return 0;
}
