#include "swayipc.hpp"

#include <cstdlib>
#include <cstring>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/unistd.h>


namespace swayipc {
	SwayIPC_Message::SwayIPC_Message() noexcept
		: m_content{'i', '3', '-', 'i', 'p', 'c'}
	{}

	SwayIPC_Message::SwayIPC_Message(SwayIPC_MessageType type) noexcept
		: SwayIPC_Message()
	{
		std::uint32_t header[2] = { 0, static_cast<std::uint32_t>(type) };

		std::uint8_t *pushdata = (std::uint8_t*)header;

		for(std::size_t i = 0; i < sizeof(header); i++) {
			m_content.push_back(pushdata[i]);
		}
	}

	SwayIPC_Message::SwayIPC_Message(SwayIPC_MessageType type, const std::vector<std::uint8_t> &data) noexcept 
		: SwayIPC_Message()
	{
		std::uint32_t header[2] = { static_cast<std::uint32_t>(data.size()), static_cast<std::uint32_t>(type) };

		std::uint8_t *pushdata = (std::uint8_t*)header;

		for(std::size_t i = 0; i < sizeof(header); i++) {
			m_content.push_back(pushdata[i]);
		}

		for(auto &c : data) {
			m_content.push_back(c);
		}
	}

	const std::vector<std::uint8_t> &SwayIPC_Message::get_content() const noexcept {
		return m_content;
	}

	SwayIPC::SwayIPC() noexcept
		: m_ready{false}, m_written{false}, m_socket{-1}
	{
		// Getting SWAYSOCK environment variable.
		char *sway_ipc_path = std::getenv("SWAYSOCK");

		if(sway_ipc_path == nullptr || strlen(sway_ipc_path) == 0) {
			return;
		}

		// Creating socket.
		int x = socket(AF_LOCAL, SOCK_STREAM, 0);

		if(x < 0) {
			return;
		}

		// Creating address and connecting the socket.
		struct sockaddr_un addr;
		memset(&addr, 0, sizeof(sockaddr_un));
		addr.sun_family = AF_LOCAL;
		strncpy(addr.sun_path, sway_ipc_path, 107);

		if(connect(x, (struct sockaddr*)&addr, sizeof(struct sockaddr_un)) < 0) {
			close(x);
			return;
		}

		m_socket = x;

		// Alles sind gut! (at least I hope that's how you write it ...)
		m_ready = true;
	}

	SwayIPC::~SwayIPC() noexcept {
		// DESTRUCTION!!!
		if(m_socket >= 0) {
			// Socket is non-negative integer if it was created successfully.
			close(m_socket);
		}
	}

	void SwayIPC::operator << (const SwayIPC_Message &message) {
		if(!m_ready) {
			return;
		}
		const std::vector<std::uint8_t> &msg = message.get_content();

		if(!write(m_socket, msg.data(), msg.size())) {
			return;
		}

		m_written = true;
	}

	void SwayIPC::operator >> (SwayIPC_Response &res) {
		if(!m_ready || !m_written) {
			return;
		}
		m_written = false;

		char header_buf[14];

		if(read(m_socket, header_buf, 14) != 14) {
			return;
		}

		if(strncmp(header_buf, "i3-ipc", 6) != 0) {
			return;
		}

		std::uint32_t length = *((std::uint32_t*)(header_buf + 6));
		SwayIPC_MessageType type = *((SwayIPC_MessageType*)(header_buf + 10));

		auto content = std::vector<std::uint8_t>{};
		content.resize(length);
		if(read(m_socket, content.data(), length) != length) {
			return;
		}

		res.type = type;
		res.length = length;
		res.content = std::move(content);
	}

	bool SwayIPC::is_ready() const noexcept {
		return m_ready;
	}
}
