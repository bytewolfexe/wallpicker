{
	description = "Random wallpaper selection for sway and i3!";

	inputs = {
		nixpkgs.url = "nixpkgs";
	};

	outputs = { self, nixpkgs }:
	let
		system = "x86_64-linux";
		pkgs = import nixpkgs { inherit system; };
	in
	{
		packages.${system}.default = pkgs.stdenv.mkDerivation {
			name = "wallpicker";

			version = "0.0.1";

			src = ./.;

			doCheck = false;

			buildPhase = ''
			g++ -std=c++20 -o wallpicker src/main.cpp src/swayipc.cpp -I ./include
			'';

			installPhase = ''
			mkdir -p $out/bin
			cp ./wallpicker $out/bin/wallpicker
			'';
		};

		devShells.${system}.default = pkgs.mkShell {
			nativeBuildInputs = with pkgs; [ bear ];
		};
	};
}
